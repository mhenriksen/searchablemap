package main

import (
	"bufio"
	"embed"
	"fmt"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const needleCount = 15

// dictData contains a list of random words borrowed from the go-fakeit project.
//
//go:embed dict.txt
var dictData embed.FS

// identTmpls contains templates for idents.
//
// 'Word' is replaced by a random capitalized word.
// 'word' is replaced by a random lowercased word.
var identTmpls = []string{
	"WordWord",
	"WordWordWord",
	"WordWordWordWord",
	"wordWord",
	"wordWordWord",
	"wordWordWordWord",
}

// needleTmpls contains templates for special idents guaranteed to be present
// in the generated list.
//
// These idents are intended as stable search targets to use in testing.
//
// The list will contain 5 of each template where 'Word' is replaced by a random
// capitalized word to ensure uniqueness.
var needleTmpls = []string{
	"haystackNeedleWord",
	"needleHaystackWord",
	"haystackWordNeedle",
}

var (
	capRegexp = regexp.MustCompile(`Word`)
	lowRegexp = regexp.MustCompile(`word`)
)

var seenMap map[string]struct{}

func main() {
	if len(os.Args) == 1 {
		fmt.Printf("usage: %s <number of idents to generate>\n", os.Args[0])
		os.Exit(1)
	}

	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		log.Fatalf("error converting %v to integer: %v", os.Args[1], err)
	}

	d, err := loadDict()
	if err != nil {
		log.Fatal(err)
	}

	seenMap = make(map[string]struct{}, n)

	for i := 0; i < needleCount/len(needleTmpls); i++ {
		for _, tmpl := range needleTmpls {
			fmt.Println(uniqIdent(tmpl, d))
		}
	}

	if n <= needleCount {
		os.Exit(0)
	}

	n -= needleCount

	tmplLen := len(identTmpls)

	for i := 0; i < n; i++ {
		tmpl := identTmpls[rand.Intn(tmplLen)]

		fmt.Println(uniqIdent(tmpl, d))
	}
}

type dict struct {
	words []string
	len   int
}

func loadDict() (*dict, error) {
	f, err := dictData.Open("dict.txt")
	if err != nil {
		return nil, fmt.Errorf("opening embedded dictionary file: %w", err)
	}
	defer f.Close()

	d := &dict{}
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		w := strings.TrimSpace(scanner.Text())
		if w == "" {
			continue
		}

		d.words = append(d.words, w)
	}

	d.len = len(d.words)

	return d, nil
}

func (d *dict) word() string {
	i := rand.Intn(d.len)
	return d.words[i]
}

func (d *dict) capitalized() string {
	w := d.word()

	return strings.ToUpper(w[:1]) + w[1:]
}

func (d *dict) lowercased() string {
	return strings.ToLower(d.word())
}

func (d *dict) fromTemplate(tmpl string) string {
	res := capRegexp.ReplaceAllStringFunc(tmpl, func(s string) string {
		return d.capitalized()
	})

	res = lowRegexp.ReplaceAllStringFunc(res, func(s string) string {
		return d.lowercased()
	})

	return res
}

func uniqIdent(tmpl string, d *dict) string {
	for {
		ident := d.fromTemplate(tmpl)
		if _, seen := seenMap[ident]; seen {
			continue
		}

		seenMap[ident] = struct{}{}

		return ident
	}
}
