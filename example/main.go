package main

import (
	"bytes"
	_ "embed"
	"encoding/csv"
	"fmt"
	"log"

	"gitlab.com/mhenriksen/searchablemap"
)

//go:embed airports.csv
var airportsCSVData []byte

func main() {
	airports, err := mapFromCSVData()
	if err != nil {
		log.Fatal(err)
	}

	sMap := searchablemap.New(airports)

	res, _ := sMap.Contains("Port", -1)
	printResult("Airports containing 'Port'", res)

	res, _ = sMap.HasPrefix("Fort", -1)
	printResult("Airports starting with 'Fort'", res)

	res, _ = sMap.HasSuffix("field", -1)
	printResult("Airports ending with 'field'", res)

	res, _ = sMap.Match(`\b(F|P)ort\b`, -1)
	printResult("Airports matching 'Port' or 'Fort'", res)
}

func printResult(desc string, airports map[string]any) {
	fmt.Printf("%s:\n\n", desc)

	for name, code := range airports {
		fmt.Printf("\t%s (%s)\n", name, code)
	}

	fmt.Print("\n")
}

func mapFromCSVData() (map[string]any, error) {
	r := csv.NewReader(bytes.NewReader(airportsCSVData))
	r.FieldsPerRecord = 2
	r.ReuseRecord = true

	records, err := r.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading records from embedded CSV data: %w", err)
	}

	res := make(map[string]any, len(records))

	for _, record := range records {
		name, code := record[0], record[1]

		res[name] = code
	}

	return res, nil
}
