package searchablemap_test

import (
	"bufio"
	"errors"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"testing"

	"gitlab.com/mhenriksen/searchablemap"
)

var expectedErrors = []error{
	searchablemap.ErrKeyIndexOutOfRange,
	searchablemap.ErrInvalidKeyIndex,
	searchablemap.InvalidSearchValError{},
}

func FuzzMap_MatchFast(f *testing.F) {
	seeds := []string{
		`needle.+`,
		`.+Needle.+`,
		`(Haystack)?Needle`,
		`x.y.z.*`,
		`abc(def|ghi)*`,
	}

	for _, s := range seeds {
		f.Add(s)
	}

	sMap := buildSMap(f)

	f.Fuzz(func(t *testing.T, expr string) {
		// TODO: Match method those not support start of line anchors, and it
		// needs to handle that situation better.
		if strings.HasPrefix(expr, "^") {
			t.Skipf("skipping expression with start of line anchor")
		}

		_, err := regexp.Compile(expr)
		if err != nil {
			t.Skipf("skipping invalid expression `%s`", expr)
		}

		fastRes, err := sMap.MatchFast(expr, -1)
		checkError(t, err, "MatchFast")

		slowRes, err := sMap.Match(expr, -1)
		checkError(t, err, "Match")

		if len(fastRes) != len(slowRes) || !reflect.DeepEqual(fastRes, slowRes) {
			t.Fatalf("MatchFast and Match results are not equal for pattern `%s`\n\nMatch:\n\n\t%#v\n\nMatchFast:\n\n\t%#v\n\n",
				expr, slowRes, fastRes,
			)
		}
	})
}

func buildSMap(tb testing.TB) *searchablemap.Map {
	tb.Helper()

	f, err := os.Open(filepath.Join("testdata", "rand-idents-fuzz.txt"))
	if err != nil {
		tb.Fatalf("error opening testdata file: %v", err)
	}
	defer f.Close()

	data := make(map[string]any, 50)
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := string(scanner.Bytes())
		data[line] = struct{}{}
	}

	return searchablemap.New(data)
}

func checkError(tb testing.TB, err error, ident string) {
	tb.Helper()

	if err == nil {
		return
	}

	for _, expected := range expectedErrors {
		if errors.Is(err, expected) {
			tb.Skipf("[%s] skipping fuzz test case causing expected error: %v", ident, err)
			return
		}
	}

	tb.Fatalf("[%s] unexpected error: %v", ident, err)
}
