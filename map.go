package searchablemap

import (
	"bytes"
	"errors"
	"fmt"
	"index/suffixarray"
	"regexp"
	"strings"
	"sync"

	"gitlab.com/mhenriksen/searchablemap/reparse"
)

// keyDelim is the delimiter used between map keys when concatenated into a
// string.
const keyDelim byte = 0x00

// minSearchValLen is the minimum length of a search value to be considered
// valid.
const minSearchValLen = 2

var keyDelimStr = fmt.Sprintf("\\x%02x", keyDelim)

var emptyResults map[string]any

var (
	// ErrKeyIndexOutOfRange is returned if a search result index does not
	// point to a map key in the internal suffix array.
	ErrKeyIndexOutOfRange = errors.New("key index out of range")
	// ErrInvalidKeyIndex is returned if a search result index is invalid.
	ErrInvalidKeyIndex = errors.New("invalid key index")
)

// InvalidSearchValError is returned by [Map] methods if receiving an invalid
// search value.
type InvalidSearchValError struct {
	Err error
	Val string
}

func invalidSearchValErrorf(val, format string, args ...any) *InvalidSearchValError {
	return &InvalidSearchValError{
		Err: fmt.Errorf(format, args...),
		Val: val,
	}
}

// Error returns the error text.
func (e InvalidSearchValError) Error() string {
	return fmt.Sprintf("invalid search value: %v", e.Err)
}

// Unwrap returns the wrapped error.
func (e InvalidSearchValError) Unwrap() error {
	return e.Err
}

// Is returns true if the provided error is an [InvalidSearchValError].
func (InvalidSearchValError) Is(target error) bool {
	if _, ok := target.(InvalidSearchValError); ok {
		return true
	}

	return false
}

// Map is a data structure that wraps a `map[string]interface{}` and offers
// efficient key-based search methods.
//
// Map leverages a suffix array to enable high-performance key lookups, even for
// large maps. Additionally, the [Map.MatchFast] method optimizes regular
// expression matching by extracting literal string patterns from the expression
// to reduce the data with basic lookup methods before regular expression
// matching.
type Map struct {
	data    map[string]any
	sa      *suffixarray.Index
	dataLen int
	saLen   int
	mu      sync.RWMutex
}

// New constructs a new searchable map wrapping the provided map.
func New(data map[string]any) *Map {
	m := &Map{data: data}

	var b []byte

	for key := range data {
		b = append(b, append([]byte(key), keyDelim)...)
	}

	m.sa = suffixarray.New(b)
	m.saLen = len(b)
	m.dataLen = len(m.data)

	return m
}

// Add inserts a new key-value pair into the map.
//
// NOTE: This method recomputes the internal index on each call, so use it
// judiciously to avoid unnecessary overhead.
func (m *Map) Add(key string, val any) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	if _, found := m.data[key]; found {
		return fmt.Errorf("key %q is already in use", key)
	}

	m.data[key] = val
	m.sa = suffixarray.New(append(m.sa.Bytes(), append([]byte(key), keyDelim)...))
	m.saLen = len(m.sa.Bytes())

	return nil
}

// Contains returns a map containing keys that contain the provided substring.
//
// If n is set to -1, all matching results are returned; otherwise, the results
// are limited to the specified number.
//
// NOTE: This method is case-sensitive.
func (m *Map) Contains(substr string, n int) (map[string]any, error) {
	if err := validateSearchString(substr); err != nil {
		return nil, err
	}

	m.mu.RLock()
	defer m.mu.RUnlock()

	indices := m.sa.Lookup([]byte(substr), n)
	iLen := len(indices)

	if iLen == 0 {
		return emptyResults, nil
	}

	res := make(map[string]any, iLen)

	for _, index := range indices {
		key, err := m.keyAtIndex(index)
		if err != nil {
			return nil, invalidSearchValErrorf(substr, "getting map key: %w", err)
		}

		val, err := m.valForKey(key)
		if err != nil {
			return nil, invalidSearchValErrorf(substr, "getting map value: %w", err)
		}

		res[key] = val
	}

	if len(res) == 0 {
		return emptyResults, nil
	}

	return res, nil
}

// HasPrefix returns a map containing keys that start with the provided prefix.
//
// If n is set to -1, all matching results are returned; otherwise, the results
// are limited to the specified number.
//
// NOTE: This method is case-sensitive.
func (m *Map) HasPrefix(prefix string, n int) (map[string]any, error) {
	if err := validateSearchString(prefix); err != nil {
		return nil, err
	}

	m.mu.RLock()
	defer m.mu.RUnlock()

	indices := m.sa.Lookup(append([]byte{keyDelim}, []byte(prefix)...), n)
	iLen := len(indices)

	if iLen == 0 {
		return emptyResults, nil
	}

	res := make(map[string]any, iLen)

	for _, index := range indices {
		key, err := m.keyAtIndex(index + 1)
		if err != nil {
			return nil, invalidSearchValErrorf(prefix, "getting map key: %w", err)
		}

		val, err := m.valForKey(key)
		if err != nil {
			return nil, invalidSearchValErrorf(prefix, "getting map value: %w", err)
		}

		res[key] = val
	}

	if len(res) == 0 {
		return emptyResults, nil
	}

	return res, nil
}

// HasSuffix returns a map containing keys that end with the provided suffix.
//
// If n is set to -1, all matching results are returned; otherwise, the results
// are limited to the specified number.
//
// NOTE: This method is case-sensitive.
func (m *Map) HasSuffix(suffix string, n int) (map[string]any, error) {
	if err := validateSearchString(suffix); err != nil {
		return nil, err
	}

	m.mu.RLock()
	defer m.mu.RUnlock()

	indices := m.sa.Lookup(append([]byte(suffix), keyDelim), n)
	iLen := len(indices)

	if iLen == 0 {
		return emptyResults, nil
	}

	res := make(map[string]any, iLen)

	for _, index := range indices {
		key, err := m.keyAtIndex(index)
		if err != nil {
			return nil, invalidSearchValErrorf(suffix, "getting map key: %w", err)
		}

		val, err := m.valForKey(key)
		if err != nil {
			return nil, invalidSearchValErrorf(suffix, "getting map value: %w", err)
		}

		res[key] = val
	}

	if len(res) == 0 {
		return emptyResults, nil
	}

	return res, nil
}

// Match returns a map containing keys that match the provided regular
// expression pattern.
//
// This method uses the suffix array index to find matching keys. Note that it
// can be slow for certain complex patterns. See [Map.MatchFast].
//
// If n is set to -1, all matching results are returned; otherwise, the results
// are limited to the specified number.
//
// NOTE: The expression will be modified to match within the boundary of a
// single map key. Patterns like `.*` and `.+` are replaced with more
// restrictive matchers. If the expression matches out of bounds, an
// InvalidSearchValError is returned.
func (m *Map) Match(expr string, n int) (map[string]any, error) {
	// Check if expression has a literal prefix.
	if prfx, valid, complete := exprLiteralPrefix(expr); prfx != "" {
		if valid && complete {
			// If expression is simply matching a valid literal prefix, it's
			// more efficient to use [Map.HasPrefix].
			return m.HasPrefix(prfx, n)
		}
	}

	p, err := boundedRegexp(expr)
	if err != nil {
		return nil, err
	}

	return m.doMatch(p, n)
}

// MatchFast is an optimized method for matching keys using regular expressions.
//
// It provides significant performance improvements for certain regular
// expressions. This method extracts literal strings within the provided
// regular expression and utilizes [Map.HasPrefix], [Map.Contains], and
// [Map.HasSuffix] methods to filter the data efficiently before performing
// regular expression matching. The performance gain is substantial for specific
// patterns.
//
// However, it's worth noting that benchmarking tests have revealed that
// Map.Match can be faster than MatchFast if the expression has a literal
// prefix. MatchFast automatically detects such cases and switches to
// [Map.Match] for optimal performance.
//
// NOTE: The regular expression expression will be modified internally to ensure
// matching occurs within the boundary of a single map key. Patterns like `.*`
// and `.+` are replaced with more restrictive matchers. If the expression
// matches out of bounds, an InvalidSearchValError is returned.
func (m *Map) MatchFast(expr string, n int) (map[string]any, error) {
	// Check if expression has a literal prefix.
	if prfx, valid, complete := exprLiteralPrefix(expr); prfx != "" {
		if valid && complete {
			// If expression is simply matching a valid literal prefix, it's
			// more efficient to use [Map.HasPrefix].
			return m.HasPrefix(prfx, n)
		}

		p, err := boundedRegexp(expr)
		if err != nil {
			return nil, err
		}

		// If the expression has a prefix, it's more efficient to use the
		// expression matching provided by suffixtree.
		return m.doMatch(p, n)
	}

	p, err := boundedRegexp(expr)
	if err != nil {
		return nil, err
	}

	literals, err := reparse.ExtractLiterals(expr, 3)
	if err != nil {
		if errors.Is(err, reparse.UnsupportedExprError{}) {
			return nil, &InvalidSearchValError{Err: err, Val: expr}
		}

		return nil, fmt.Errorf("extracting literals in pattern: %w", err)
	}

	if len(literals) == 0 {
		return m.doMatch(p, n)
	}

	data, err := searchWithLit(m, literals[0])
	if err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return emptyResults, nil
	}

	sMap := New(data)

	for _, lit := range literals[1:] {
		data, err := searchWithLit(sMap, lit)
		if err != nil {
			return nil, err
		}

		if len(data) == 0 {
			return emptyResults, nil
		}

		sMap = New(data)
	}

	return sMap.doMatch(p, n)
}

func (m *Map) doMatch(p *regexp.Regexp, n int) (map[string]any, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	indices := m.sa.FindAllIndex(p, n)
	iLen := len(indices)

	if iLen == 0 {
		return emptyResults, nil
	}

	res := make(map[string]any, iLen)

	for _, index := range indices {
		match := m.sa.Bytes()[index[0]:index[1]]
		if bytes.Contains(match, []byte{keyDelim}) {
			return nil, invalidSearchValErrorf(p.String(), "expression matched out of bounds")
		}

		key, err := m.keyAtIndex(index[0])
		if err != nil {
			return nil, fmt.Errorf("getting map key: %w", err)
		}

		val, err := m.valForKey(key)
		if err != nil {
			return nil, fmt.Errorf("getting map value: %w", err)
		}

		res[key] = val
	}

	if len(res) == 0 {
		return emptyResults, nil
	}

	return res, nil
}

func (m *Map) keyAtIndex(index int) (string, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	if index >= m.saLen || index < 0 {
		return "", ErrKeyIndexOutOfRange
	}

	data := m.sa.Bytes()
	start := index
	end := index

	for start >= 0 && data[start] != keyDelim {
		start--
	}

	for end < m.saLen && data[end] != keyDelim {
		end++
	}

	if start == end || start+1 >= end {
		return "", ErrInvalidKeyIndex
	}

	return string(data[start+1 : end]), nil
}

func (m *Map) valForKey(key string) (any, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	val, ok := m.data[key]
	if !ok {
		return "", fmt.Errorf("unknown map key %q", key)
	}

	return val, nil
}

func validateSearchString(s string) error {
	if len(s) < minSearchValLen {
		return invalidSearchValErrorf(s, "search value length must be at least %d", minSearchValLen)
	}

	if strings.Contains(s, string(keyDelim)) {
		return invalidSearchValErrorf(s, "search value cannot contain special key dilimeter value (%s)", keyDelimStr)
	}

	return nil
}

func boundedRegexp(expr string) (*regexp.Regexp, error) {
	if err := validateSearchString(expr); err != nil {
		return nil, err
	}

	mod := reparse.BoundedRegexp(expr, keyDelimStr)

	p, err := regexp.Compile(mod)
	if err != nil {
		return nil, invalidSearchValErrorf(mod, "compiling bounded regular expression: %w", err)
	}

	return p, nil
}

// exprLiteralPrefix checks if a regular expression has a literal prefix and
// returns it together valid and complete boolean flags.
//
// Returns:
//
//   - prfx: the literal prefix if present (empty string if not).
//   - valid: true if prfx is a valid search value.
//   - complete: true if expression has Start Of Line (`^`) anchor AND the
//     literal prefix comprises the entire pattern.
//
// If valid and complete is true, it's possible to search with the more
// efficient [Map.HasPrefix] method instead of performing regular expression
// matching.
func exprLiteralPrefix(expr string) (prfx string, valid, complete bool) {
	// Check if expression has a start of string anchor.
	expr, hasStartOfString := strings.CutPrefix(expr, "^")

	p, err := regexp.Compile(expr)
	if err != nil {
		return "", false, false
	}

	prfx, complete = p.LiteralPrefix()
	if prfx == "" {
		return "", false, false
	}

	valid = validateSearchString(prfx) == nil

	return prfx, valid, (complete && hasStartOfString)
}

func searchWithLit(m *Map, lit reparse.Token) (data map[string]any, err error) {
	switch lit.Kind {
	case reparse.Prefix:
		data, err = m.HasPrefix(lit.Value, -1)
	case reparse.Suffix:
		data, err = m.HasSuffix(lit.Value, -1)
	case reparse.Substr:
		data, err = m.Contains(lit.Value, -1)
	default:
		return nil, fmt.Errorf("unexpected token kind %s", lit)
	}

	if err != nil {
		return nil, fmt.Errorf("reducing data with literal %#v: %w", lit, err)
	}

	return data, nil
}
