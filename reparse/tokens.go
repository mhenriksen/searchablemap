package reparse

import (
	"fmt"
	"strings"
)

type TokenKind int

const (
	Prefix TokenKind = iota + 1
	Suffix
	Substr
)

func (tk TokenKind) String() string {
	return map[TokenKind]string{
		Substr: "Substr",
		Prefix: "Prefix",
		Suffix: "Suffix",
	}[tk]
}

type Token struct {
	Value string
	Kind  TokenKind
}

func NewToken(kind TokenKind, value string) Token {
	return Token{Value: value, Kind: kind}
}

func (t Token) Less(other Token) bool {
	if t.Kind < other.Kind {
		return true
	} else if other.Kind < t.Kind {
		return false
	}

	return t.Value > other.Value
}

func (t Token) Equal(other Token) bool {
	return t.Kind == other.Kind && t.Value == other.Value
}

func (t Token) String() string {
	return t.Value
}

func (t Token) GoString() string {
	return fmt.Sprintf("%s(%q)", t.Kind, t.Value)
}

type tokenBuilder struct {
	value  []rune
	kind   TokenKind
	repeat int
}

func newTokenBuilder() *tokenBuilder {
	return &tokenBuilder{kind: Substr}
}

func (tb *tokenBuilder) setKind(k TokenKind) *tokenBuilder {
	tb.kind = k
	return tb
}

func (tb *tokenBuilder) setValue(val []rune) *tokenBuilder {
	tb.value = val
	return tb
}

func (tb *tokenBuilder) setMinRepeat(n int) *tokenBuilder {
	tb.repeat = n
	return tb
}

func (tb *tokenBuilder) token() Token {
	t := Token{Kind: tb.kind, Value: string(tb.value)}

	if tb.repeat > 1 {
		t.Value = strings.Repeat(t.Value, tb.repeat)
	}

	tb.reset()

	return t
}

func (tb *tokenBuilder) reset() {
	tb.kind = Substr
	tb.value = nil
	tb.repeat = 0
}
