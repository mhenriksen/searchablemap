package reparse

import (
	"fmt"
	"regexp/syntax"
	"strings"
)

var skipOp = map[syntax.Op]struct{}{
	syntax.OpAlternate: {},
	syntax.OpCharClass: {},
	syntax.OpStar:      {},
	syntax.OpQuest:     {},
}

type iter struct {
	patterns []*syntax.Regexp
	len      int
	pos      int
}

func newIter(re *syntax.Regexp) *iter {
	patterns := flattenRe(re)

	return &iter{
		patterns: patterns,
		len:      len(patterns),
	}
}

func (i *iter) next() (*syntax.Regexp, bool) {
	re, ok := i.peek()
	if !ok {
		return nil, false
	}

	i.pos++

	return re, true
}

func (i *iter) peek() (*syntax.Regexp, bool) {
	if i.pos >= i.len {
		return nil, false
	}

	return i.patterns[i.pos], true
}

func (it iter) GoString() string {
	var b strings.Builder

	fmt.Fprintf(&b, "iter{pos=\"%d\" len=\"%d\"", it.pos, it.len)

	for i, p := range it.patterns {
		fmt.Fprintf(&b, " p%d=\"%s(%v)\"", i, p.Op, p)
	}

	b.WriteRune('}')

	return b.String()
}

func flattenRe(re *syntax.Regexp) []*syntax.Regexp {
	var patterns []*syntax.Regexp

	if _, skip := skipOp[re.Op]; skip {
		return patterns
	}

	patterns = append(patterns, re)

	for _, sub := range re.Sub {
		patterns = append(patterns, flattenRe(sub)...)
	}

	return patterns
}
