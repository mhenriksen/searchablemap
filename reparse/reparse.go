package reparse

import (
	"fmt"
	"regexp/syntax"
	"sort"
	"strings"
)

type UnsupportedExprError struct {
	Err  error
	Expr string
}

func unsupportedExprErrorf(expr, format string, args ...any) *UnsupportedExprError {
	return &UnsupportedExprError{
		Err:  fmt.Errorf(format, args...),
		Expr: expr,
	}
}

func (e UnsupportedExprError) Error() string {
	return fmt.Sprintf("unsupported expression: %v", e.Err)
}

func (e UnsupportedExprError) Unwrap() error {
	return e.Err
}

func (UnsupportedExprError) Is(target error) bool {
	if _, ok := target.(UnsupportedExprError); ok {
		return true
	}

	return false
}

// ExtractLiterals parses a regular expression and returns literal strings
// within the pattern with length >= minLength.
func ExtractLiterals(expr string, minLength int) ([]Token, error) {
	re, err := syntax.Parse(expr, syntax.Perl)
	if err != nil {
		return nil, fmt.Errorf("parsing regular expression pattern: %w", err)
	}

	if (re.Flags & syntax.FoldCase) != 0 {
		return nil, unsupportedExprErrorf(expr, "cannot extract literals from case-insensitive expression")
	}

	var tokens []Token

	patterns := newIter(re)
	builder := newTokenBuilder()

	for {
		p, ok := patterns.next()
		if !ok {
			break
		}

		switch p.Op {
		case syntax.OpBeginLine, syntax.OpBeginText:
			builder.setKind(Prefix)
		case syntax.OpRepeat:
			builder.setMinRepeat(p.Min)
		case syntax.OpLiteral:
			builder.setValue(p.Rune)

			if peek, ok := patterns.peek(); ok && opOneOf(peek.Op, syntax.OpEndLine, syntax.OpEndText) {
				builder.setKind(Suffix)
			}

			token := builder.token()
			if len(token.Value) < minLength {
				continue
			}

			tokens = append(tokens, token)
		}
	}

	return uniqSort(tokens), nil
}

// BoundedRegexp adjusts a regular expression to match only within a specified
// boundary.
//
// NOTE: the boundary enforcement is not guaranteed.
func BoundedRegexp(expr, delim string) string {
	boundary := fmt.Sprintf("[^%s]", delim)
	replacer := strings.NewReplacer(
		".*", boundary,
		".+", boundary,
		".{", boundary+"{", // `.{x,y}` => `[^<delim>]{x,y}`.
	)

	return replacer.Replace(expr)
}

func uniqSort(tokens []Token) []Token {
	tMap := make(map[Token]struct{})
	uniq := make([]Token, 0, len(tokens))

	for _, tok := range tokens {
		if _, seen := tMap[tok]; seen {
			continue
		}

		tMap[tok] = struct{}{}

		uniq = append(uniq, tok)
	}

	sort.SliceStable(uniq, func(i, j int) bool {
		return uniq[i].Less(uniq[j])
	})

	return uniq
}

func opOneOf(actual syntax.Op, ops ...syntax.Op) bool {
	for _, op := range ops {
		if actual == op {
			return true
		}
	}

	return false
}
