package reparse_test

import (
	"reflect"
	"testing"

	"gitlab.com/mhenriksen/searchablemap/reparse"
)

const defaultMinLen = 3

func TestExtractLiterals(t *testing.T) {
	tt := []struct {
		name   string
		p      string
		minLen int
		want   []reparse.Token
	}{
		{
			name: "simple substring",
			p:    `abcd?`,
			want: []reparse.Token{substr(t, "abc")},
		},
		{
			name: "simple prefix",
			p:    `^abcd?`,
			want: []reparse.Token{prefix(t, "abc")},
		},
		{
			name: "simple suffix",
			p:    `a?bcd$`,
			want: []reparse.Token{suffix(t, "bcd")},
		},
		{
			name: "no literals",
			p:    `[A-Za-z]{8,32}`,
			want: []reparse.Token{},
		},
		{
			name: "literal expansion",
			p:    "^(abc){2}",
			want: []reparse.Token{prefix(t, "abcabc")},
		},
		{
			name: "literal expansion with max",
			p:    "(123){3,5}",
			want: []reparse.Token{substr(t, "123123123")},
		},
		{
			name: "prefix, substr, and suffix",
			p:    `^Assert.+Not.+OrNil$`,
			want: []reparse.Token{prefix(t, "Assert"), suffix(t, "OrNil"), substr(t, "Not")},
		},
		{
			name: "alternate literals are skipped",
			p:    `abc(def|ghi)(jkl){3}`,
			want: []reparse.Token{substr(t, "jkljkljkl"), substr(t, "abc")},
		},
		{
			name: "star literals are skipped",
			p:    `abc(123)*`,
			want: []reparse.Token{substr(t, "abc")},
		},
		{
			name: "optional literals are skipped",
			p:    `123(abc)?`,
			want: []reparse.Token{substr(t, "123")},
		},
		{
			name: "space is considered part of a literal",
			p:    `a b c .+`,
			want: []reparse.Token{substr(t, "a b c ")},
		},
	}

	for _, tc := range tt {
		tc := tc

		if tc.minLen == 0 {
			tc.minLen = defaultMinLen
		}

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := reparse.ExtractLiterals(tc.p, tc.minLen)
			if err != nil {
				t.Fatalf("did not expect error, but got: %v", err)
			}

			if reflect.DeepEqual(actual, tc.want) {
				return
			}

			t.Fatalf("result does not match expected\n\nEXPECTED:\n\n\t%#v\n\nACTUAL:\n\n\t%#v\n\n", tc.want, actual)
		})
	}
}

func substr(tb testing.TB, val string) reparse.Token {
	tb.Helper()

	return reparse.NewToken(reparse.Substr, val)
}

func prefix(tb testing.TB, val string) reparse.Token {
	tb.Helper()

	return reparse.NewToken(reparse.Prefix, val)
}

func suffix(tb testing.TB, val string) reparse.Token {
	tb.Helper()

	return reparse.NewToken(reparse.Suffix, val)
}
