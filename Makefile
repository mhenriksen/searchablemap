DBG_MAKEFILE ?=
ifeq ($(DBG_MAKEFILE),1)
    $(warning ***** starting Makefile for goal(s) "$(MAKECMDGOALS)")
    $(warning ***** $(shell date))
else
    MAKEFLAGS += -s
endif

DBG ?=

MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --warn-undefined-variables
.SUFFIXES:

GOBIN ?= go
ARGS ?=
PKG ?= ./...
TESTMATCH ?= .
BENCHMATCH ?= .
FUZZMATCH ?= .

default: test bench

test:
	$(GOBIN) test -shuffle=on -cover -covermode=atomic -race -run='$(TESTMATCH)' $(ARGS) $(PKG)

bench:
	$(GOBIN) test -bench='$(BENCHMATCH)' -benchmem -count=3 $(ARGS) $(PKG)

fuzz:
	$(GOBIN) test -fuzz='$(FUZZMATCH)' $(ARGS) $(PKG)

.PHONY: profile
profile:
	rm -rf ./profile && mkdir ./profile
	$(GOBIN) test -bench='$(BENCHMATCH)' -benchmem -memprofile profile/mem.out -cpuprofile profile/cpu.out $(ARGS) $(PKG)
	$(GOBIN) tool pprof -output profile/mem.svg -web profile/mem.out
	$(GOBIN) tool pprof -output profile/cpu.svg -web profile/cpu.out
