# searchablemap

> ## :warning: CAUTION
>
> This is currently an experimental package for research purposes. Do not use.

searchablemap is a Go package that provides efficient key-based search capabilities for a `map[string]interface{}` data
structure. It achieves high-performance key lookups, even for large maps, by leveraging a suffix array. Additionally,
it optimizes regular expression matching by extracting literal string patterns from expressions and reducing the data
with basic lookup methods before performing regular expression matching. This optimization ensures that expensive
regular expression matching is only performed on keys that are likely to match, and offers a very significant
performance improvement for complex regular expressions.

## Usage

See [example/main.go](/example/main.go) for a usage example. Run it with:

```console
user@example:~/src$ go run example/main.go
Airports containing 'Port':

        Port Largo (KYL)
        Port Meadville (MEJ)
        Portage Creek (PCA)
        Portland International Jetport (PWM)
        Porto Alegre (PGP)
        Chuja Port Heliport (JCJ)
        Port Williams Seaplane Base (KPR)
        Porterville Municipal (PTV)
        Port Simpson Seaplane Base (YPI)

Airports starting with 'Fort':

        Fort Chipewyan (YPY)
        Fort Mackay / Firebag (YFI)
        Fort Madison Municipal (FMS)
        Fort Reliance Seaplane Base (YFL)
        Fortescue - Dave Forrest Aerodrome (KFE)

Airports ending with 'field':

        Decatur HI-Way Airfield (DCR)
        Kornasoren Airfield (FOO)
        Wake Island Airfield (AWK)
        Biała Podlaska Airfield (BXP)
        Wheeler Army Airfield (HHI)
        Maxson Airfield (AXB)
        Vinh Long Airfield (XVL)

Airports matching 'Port' or 'Fort':

        Butts AAF (Fort Carson) Air Field (FCS)
        Fort Chipewyan (YPY)
        Fort Reliance Seaplane Base (YFL)
        Fort Madison Municipal (FMS)
        Port Largo (KYL)
        Henry Post Army Air Field (Fort Sill) (FSI)
        Lawton Fort Sill Regional (LAW)
        Port Meadville (MEJ)
        Chuja Port Heliport (JCJ)
        Port Williams Seaplane Base (KPR)
        Fort Mackay / Firebag (YFI)
        Port Simpson Seaplane Base (YPI)

```

## Benchmarks

Compare against a map containing 500,000 entries. In this map, the keys consist of random words concatenated in camel
case or pascal case to mimic typical `ident` names in source code. The values are [128]byte slices containing random
bytes to simulate storing some sort of useful data for each ident.

The actual test data for the benchmark can be found in [rand-idents-500k.txt](/testdata/rand-idents-500k.txt).

```console
user@example:~/src$ BENCH_SIZE=500k make bench
goos: darwin
goarch: arm64
pkg: gitlab.com/mhenriksen/searchablemap
BenchmarkMap_Contains/match_500k-10                                       928612              1361 ns/op             950 B/op         13 allocs/op
BenchmarkMap_Contains/match_500k-10                                       912266              1331 ns/op             950 B/op         13 allocs/op
BenchmarkMap_Contains/match_500k-10                                       919545              1339 ns/op             950 B/op         13 allocs/op
BenchmarkMap_Contains/no_match_500k-10                                   2283822               516.1 ns/op             0 B/op          0 allocs/op
BenchmarkMap_Contains/no_match_500k-10                                   2330796               527.5 ns/op             0 B/op          0 allocs/op
BenchmarkMap_Contains/no_match_500k-10                                   2275652               524.1 ns/op             0 B/op          0 allocs/op
BenchmarkMap_HasPrefix/match_500k-10                                     1307066               914.5 ns/op           512 B/op          9 allocs/op
BenchmarkMap_HasPrefix/match_500k-10                                     1319863               919.8 ns/op           512 B/op          9 allocs/op
BenchmarkMap_HasPrefix/match_500k-10                                     1310781               908.0 ns/op           512 B/op          9 allocs/op
BenchmarkMap_HasPrefix/no_match_500k-10                                  2311286               521.2 ns/op            16 B/op          1 allocs/op
BenchmarkMap_HasPrefix/no_match_500k-10                                  2301841               526.5 ns/op            16 B/op          1 allocs/op
BenchmarkMap_HasPrefix/no_match_500k-10                                  2321752               516.9 ns/op            16 B/op          1 allocs/op
BenchmarkMap_HasSuffix/match_500k-10                                     1359208               875.7 ns/op           504 B/op          8 allocs/op
BenchmarkMap_HasSuffix/match_500k-10                                     1371220               865.3 ns/op           504 B/op          8 allocs/op
BenchmarkMap_HasSuffix/match_500k-10                                     1369632               872.4 ns/op           504 B/op          8 allocs/op
BenchmarkMap_HasSuffix/no_match_500k-10                                  2258013               526.1 ns/op            16 B/op          1 allocs/op
BenchmarkMap_HasSuffix/no_match_500k-10                                  2290245               524.2 ns/op            16 B/op          1 allocs/op
BenchmarkMap_HasSuffix/no_match_500k-10                                  2294184               529.0 ns/op            16 B/op          1 allocs/op
BenchmarkMap_Match/match_500k-10                                               5         212939783 ns/op            9134 B/op         89 allocs/op
BenchmarkMap_Match/match_500k-10                                               5         217238200 ns/op            7980 B/op         86 allocs/op
BenchmarkMap_Match/match_500k-10                                               5         212727675 ns/op            9134 B/op         89 allocs/op
BenchmarkMap_Match/no_match_500k-10                                            5         204449642 ns/op            7718 B/op         66 allocs/op
BenchmarkMap_Match/no_match_500k-10                                            5         203585892 ns/op            7718 B/op         66 allocs/op
BenchmarkMap_Match/no_match_500k-10                                            5         203505575 ns/op            6564 B/op         63 allocs/op
BenchmarkMap_Match/with_prefix_literal_500k-10                            186144              6432 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_Match/with_prefix_literal_500k-10                            189342              6339 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_Match/with_prefix_literal_500k-10                            193096              6308 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_Match/with_complete_prefix_literal_500k-10                   723072              1702 ns/op            1896 B/op         26 allocs/op
BenchmarkMap_Match/with_complete_prefix_literal_500k-10                   680757              1741 ns/op            1896 B/op         26 allocs/op
BenchmarkMap_Match/with_complete_prefix_literal_500k-10                   709923              1694 ns/op            1896 B/op         26 allocs/op
BenchmarkMap_MatchFast/match_500k-10                                       53556             21667 ns/op           12660 B/op        143 allocs/op
BenchmarkMap_MatchFast/match_500k-10                                       53472             22636 ns/op           12664 B/op        143 allocs/op
BenchmarkMap_MatchFast/match_500k-10                                       52036             21800 ns/op           12661 B/op        143 allocs/op
BenchmarkMap_MatchFast/no_match_500k-10                                   216697              5533 ns/op            7368 B/op         94 allocs/op
BenchmarkMap_MatchFast/no_match_500k-10                                   219979              5669 ns/op            7368 B/op         94 allocs/op
BenchmarkMap_MatchFast/no_match_500k-10                                   222006              5548 ns/op            7368 B/op         94 allocs/op
BenchmarkMap_MatchFast/with_prefix_literal_500k-10                        192608              6353 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_MatchFast/with_prefix_literal_500k-10                        195555              6312 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_MatchFast/with_prefix_literal_500k-10                        188499              6402 ns/op            8369 B/op         94 allocs/op
BenchmarkMap_MatchFast/with_complete_prefix_literal_500k-10               705846              1755 ns/op            1896 B/op         26 allocs/op
BenchmarkMap_MatchFast/with_complete_prefix_literal_500k-10               707905              1722 ns/op            1896 B/op         26 allocs/op
BenchmarkMap_MatchFast/with_complete_prefix_literal_500k-10               705262              1721 ns/op            1896 B/op         26 allocs/op
PASS
ok      gitlab.com/mhenriksen/searchablemap     76.195s
?       gitlab.com/mhenriksen/searchablemap/cmd/genidents       [no test files]
?       gitlab.com/mhenriksen/searchablemap/example     [no test files]
PASS
ok      gitlab.com/mhenriksen/searchablemap/reparse     0.400s
```

_Benchmark performed on a MacBook Pro, Apple M1 Max 10 cores, 32 GB memory._

## Tests

```console
user@example:~/src$ make test
?       gitlab.com/mhenriksen/searchablemap/cmd/genidents       [no test files]
?       gitlab.com/mhenriksen/searchablemap/example     [no test files]
ok      gitlab.com/mhenriksen/searchablemap     3.370s  coverage: 78.4% of statements
ok      gitlab.com/mhenriksen/searchablemap/reparse     1.401s  coverage: 75.5% of statements
```
