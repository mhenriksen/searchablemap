package searchablemap_test

import (
	"bufio"
	"encoding/csv"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"testing"

	"gitlab.com/mhenriksen/searchablemap"
)

func TestMap_Add(t *testing.T) {
	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	add := map[string]any{
		"Marshmallow Cloudport":         "MCC",
		"Cheeseburger Landing Terminal": "CLT",
		"Area 51":                       "ALN",
	}

	for k, v := range add {
		if err := sMap.Add(k, v); err != nil {
			t.Fatalf("did not expect error when adding %s (%s), but got: %v", k, v, err)
		}
	}

	tt := []struct {
		name   string
		substr string
		want   map[string]any
	}{
		{
			"Cheeseburger Landing Terminal is found",
			"Cheeseburger",
			map[string]any{"Cheeseburger Landing Terminal": "CLT"},
		},
		{
			"Previous airports are found",
			"olo",
			map[string]any{
				"Alto Molocue":   "AME",
				"Cologne Bonn":   "CGN",
				"Colonel Hill":   "CRI",
				"Colorado Creek": "KCR",
				"Golovin":        "GLV",
				"María Dolores":  "LSQ",
				"Punta Colorada": "PCO",
				"Villa Dolores":  "VDR",
				"Vologda":        "VGD",
			},
		},
	}

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.Contains(tc.substr, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})

	}
}

func TestMap_Contains(t *testing.T) {
	tt := []struct {
		name   string
		substr string
		want   map[string]any
	}{
		{
			"olo returns 9 results",
			"olo",
			map[string]any{
				"Alto Molocue":   "AME",
				"Cologne Bonn":   "CGN",
				"Colonel Hill":   "CRI",
				"Colorado Creek": "KCR",
				"Golovin":        "GLV",
				"María Dolores":  "LSQ",
				"Punta Colorada": "PCO",
				"Villa Dolores":  "VDR",
				"Vologda":        "VGD",
			},
		},
		{
			"Port returns 9 results",
			"Port",
			map[string]any{
				"Chuja Port Heliport":            "JCJ",
				"Port Largo":                     "KYL",
				"Port Meadville":                 "MEJ",
				"Port Simpson Seaplane Base":     "YPI",
				"Port Williams Seaplane Base":    "KPR",
				"Portage Creek":                  "PCA",
				"Porterville Municipal":          "PTV",
				"Portland International Jetport": "PWM",
				"Porto Alegre":                   "PGP",
			},
		},
		{
			"Heliport returns 7 results",
			"Heliport",
			map[string]any{
				"Berkley Municipal Heliport": "JBK",
				"Chuja Port Heliport":        "JCJ",
				"East 34th Street Heliport":  "TSS",
				"Isla de Tigre  Heliport":    "ELX",
				"Nanortalik Heliport":        "JNN",
				"Narsaq Heliport":            "JNS",
				"Senipah Heliport":           "SZH",
			},
		},
		{
			"Kornasoren Airfield returns 1 result",
			"Kornasoren Airfield",
			map[string]any{
				"Kornasoren Airfield": "FOO",
			},
		},
		{
			"Norðfjör returns 1 result",
			"Norðfjör",
			map[string]any{
				"Norðfjörður": "NOR",
			},
		},
		{
			"xyz returns no results",
			"xyz",
			nil,
		},
	}

	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.Contains(tc.substr, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})
	}
}

func TestMap_HasPrefix(t *testing.T) {
	tt := []struct {
		name   string
		prefix string
		want   map[string]any
	}{
		{
			"Fort returns 5 results",
			"Fort",
			map[string]any{
				"Fort Chipewyan":                     "YPY",
				"Fort Mackay / Firebag":              "YFI",
				"Fort Madison Municipal":             "FMS",
				"Fort Reliance Seaplane Base":        "YFL",
				"Fortescue - Dave Forrest Aerodrome": "KFE",
			},
		},
		{
			"Norðfjör returns 1 result",
			"Norðfjör",
			map[string]any{
				"Norðfjörður": "NOR",
			},
		},
		{
			"xyz returns no results",
			"xyz",
			nil,
		},
	}

	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.HasPrefix(tc.prefix, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})
	}
}

func TestMap_HasSuffix(t *testing.T) {
	tt := []struct {
		name   string
		suffix string
		want   map[string]any
	}{
		{
			"field returns x results",
			"field",
			map[string]any{
				"Biała Podlaska Airfield": "BXP",
				"Decatur HI-Way Airfield": "DCR",
				"Kornasoren Airfield":     "FOO",
				"Maxson Airfield":         "AXB",
				"Vinh Long Airfield":      "XVL",
				"Wake Island Airfield":    "AWK",
				"Wheeler Army Airfield":   "HHI",
			},
		},
		{
			"fjörður returns 1 result",
			"fjörður",
			map[string]any{
				"Norðfjörður": "NOR",
			},
		},
		{
			"xyz returns no results",
			"xyz",
			nil,
		},
	}

	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.HasSuffix(tc.suffix, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})
	}
}

func TestMap_Match(t *testing.T) {
	tt := []struct {
		name string
		expr string
		want map[string]any
	}{
		{
			"Fort or Port returns 12 results",
			`\b(F|P)ort\b`,
			map[string]any{
				"Butts AAF (Fort Carson) Air Field":     "FCS",
				"Chuja Port Heliport":                   "JCJ",
				"Fort Chipewyan":                        "YPY",
				"Fort Mackay / Firebag":                 "YFI",
				"Fort Madison Municipal":                "FMS",
				"Fort Reliance Seaplane Base":           "YFL",
				"Henry Post Army Air Field (Fort Sill)": "FSI",
				"Lawton Fort Sill Regional":             "LAW",
				"Port Largo":                            "KYL",
				"Port Meadville":                        "MEJ",
				"Port Simpson Seaplane Base":            "YPI",
				"Port Williams Seaplane Base":           "KPR",
			},
		},
		{
			"x.y.z returns no results",
			`x.y.z`,
			nil,
		},
	}

	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.Match(tc.expr, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})
	}
}

func TestMap_MatchFast(t *testing.T) {
	tt := []struct {
		name string
		p    string
		want map[string]any
	}{
		{
			"Fort or Port returns 12 results",
			`\b(F|P)ort\b`,
			map[string]any{
				"Butts AAF (Fort Carson) Air Field":     "FCS",
				"Chuja Port Heliport":                   "JCJ",
				"Fort Chipewyan":                        "YPY",
				"Fort Mackay / Firebag":                 "YFI",
				"Fort Madison Municipal":                "FMS",
				"Fort Reliance Seaplane Base":           "YFL",
				"Henry Post Army Air Field (Fort Sill)": "FSI",
				"Lawton Fort Sill Regional":             "LAW",
				"Port Largo":                            "KYL",
				"Port Meadville":                        "MEJ",
				"Port Simpson Seaplane Base":            "YPI",
				"Port Williams Seaplane Base":           "KPR",
			},
		},
		{
			"x.y.z returns no results",
			`x.y.z`,
			nil,
		},
	}

	sMap := searchablemap.New(mapFromTestFile(t, "airports.csv"))

	for _, tc := range tt {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			actual, err := sMap.MatchFast(tc.p, -1)

			requireNoError(t, err)
			requireEqualResults(t, tc.want, actual)
		})
	}
}

func TestMap_MatchFastResultsEqualMatchResults(t *testing.T) {
	tt := []string{
		`needle.+`,
		`.+Needle.+`,
		`^needle.+`,
		`^needle`,
		`(Haystack)?Needle`,
		`x.y.z`,
	}

	sMap := searchablemap.New(mapFromTestFile(t, "rand-idents-50k.txt"))

	for _, tc := range tt {
		slowRes, err := sMap.Match(tc, -1)
		requireNoError(t, err)

		fastRes, err := sMap.MatchFast(tc, -1)
		requireNoError(t, err)

		requireEqualResults(t, slowRes, fastRes)
	}
}

func mapFromTestFile(tb testing.TB, name string) map[string]any {
	tb.Helper()

	f, err := os.Open(filepath.Join("testdata", name))
	if err != nil {
		tb.Fatalf("error opening testdata file: %v", err)
	}
	defer f.Close()

	if filepath.Ext(name) == ".csv" {
		r := csv.NewReader(f)
		r.FieldsPerRecord = 2
		r.ReuseRecord = true

		records, err := r.ReadAll()
		if err != nil {
			tb.Fatalf("error reading CSV records: %v", err)
		}

		res := make(map[string]any, len(records))

		for i, record := range records {
			key, val := record[0], record[1]

			if _, found := res[key]; found {
				tb.Fatalf("duplicate key in test file %s:%d: %q", name, i+1, key)
			}

			res[key] = val
		}

		return res
	}

	scanner := bufio.NewScanner(f)

	res := make(map[string]any)

	for scanner.Scan() {
		key := strings.TrimSpace(scanner.Text())
		if key == "" {
			continue
		}

		if _, found := res[key]; found {
			tb.Fatalf("duplicate key in test file %s: %q", name, key)
		}

		res[key] = struct{}{}
	}

	return res
}

func requireNoError(tb testing.TB, err error) {
	tb.Helper()

	if err != nil {
		tb.Fatalf("expected no error, but got: %v", err)
	}
}

func requireEqualResults(tb testing.TB, want, actual map[string]any) {
	tb.Helper()

	if len(want) == len(actual) && reflect.DeepEqual(want, actual) {
		return
	}

	wantKeys := make([]string, 0, len(want))
	actualKeys := make([]string, 0, len(actual))

	for key := range want {
		wantKeys = append(wantKeys, key)
	}

	for key := range actual {
		actualKeys = append(actualKeys, key)
	}

	sort.Strings(wantKeys)
	sort.Strings(actualKeys)

	tb.Fatalf("expected search results to be equal\n\nEXPECTED KEYS (%d):\n\n\t%s\n\nACTUAL KEYS (%d):\n\n\t%s\n\n",
		len(wantKeys), strings.Join(wantKeys, ", "), len(actualKeys), strings.Join(actualKeys, ", "),
	)
}

func requireResult(tb testing.TB, res map[string]any) {
	tb.Helper()

	if res == nil {
		tb.Fatalf("expected non-empty result, but was nil")
	}

	if len(res) == 0 {
		tb.Fatalf("expected non-empty result, but was empty")
	}
}

func requireEmptyResult(tb testing.TB, res map[string]any) {
	tb.Helper()

	if len(res) == 0 {
		return
	}

	keys := make([]string, 0, len(res))

	for key := range res {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	tb.Fatalf("expected empty result, but contains map keys: %s", strings.Join(keys, ", "))
}
