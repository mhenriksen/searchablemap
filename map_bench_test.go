package searchablemap_test

import (
	"bufio"
	"crypto/rand"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	"gitlab.com/mhenriksen/searchablemap"
)

func BenchmarkMap_Contains(b *testing.B) {
	sMap, size := benchmarkSMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Contains("Needle", -1)
			requireNoError(b, err)
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Contains("Benchmark", -1)
			requireNoError(b, err)
		}
	})
}

func BenchmarkMap_HasPrefix(b *testing.B) {
	sMap, size := benchmarkSMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.HasPrefix("needle", -1)
			requireNoError(b, err)
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.HasPrefix("Benchmark", -1)
			requireNoError(b, err)
		}
	})
}

func BenchmarkMap_HasSuffix(b *testing.B) {
	sMap, size := benchmarkSMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.HasSuffix("Needle", -1)
			requireNoError(b, err)
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.HasPrefix("Benchmark", -1)
			requireNoError(b, err)
		}
	})
}

func BenchmarkMap_Match(b *testing.B) {
	sMap, size := benchmarkSMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Match(`(n|N)eedle.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Match(`(b|B)enchmark.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("with prefix literal "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Match(`needle.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("with complete prefix literal "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.Match(`^needle`, -1)
			requireNoError(b, err)
		}
	})
}

func BenchmarkMap_MatchFast(b *testing.B) {
	sMap, size := benchmarkSMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.MatchFast(`(n|N)eedle.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.MatchFast(`(b|B)enchmark.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("with prefix literal "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.MatchFast(`needle.+`, -1)
			requireNoError(b, err)
		}
	})

	b.Run("with complete prefix literal "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := sMap.MatchFast(`^needle`, -1)
			requireNoError(b, err)
		}
	})
}

// BenchmarkMap_Contains_NaiveImpl implements a basic, naive approach to finding
// map keys containing a substring, for comparison.
func BenchmarkMap_Contains_NaiveImpl(b *testing.B) {
	if _, ok := os.LookupEnv("BENCH_NAIVE"); !ok {
		b.Skipf("skipping naive implementation benchmark; set env BENCH_NAIVE=1 to run")
	}

	data, size := benchmarkMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.Contains(k, "Needle") {
					res[k] = v
				}
			}
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.Contains(k, "Benchmark") {
					res[k] = v
				}
			}
		}
	})
}

// BenchmarkMap_HasPrefix_NaiveImpl implements a basic, naive approach to finding
// map keys by prefix, for comparison.
func BenchmarkMap_HasPrefix_NaiveImpl(b *testing.B) {
	if _, ok := os.LookupEnv("BENCH_NAIVE"); !ok {
		b.Skipf("skipping naive implementation benchmark; set env BENCH_NAIVE=1 to run")
	}

	data, size := benchmarkMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.HasPrefix(k, "needle") {
					res[k] = v
				}
			}
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.HasPrefix(k, "Benchmark") {
					res[k] = v
				}
			}
		}
	})
}

// BenchmarkMap_HasSuffix_NaiveImpl implements a basic, naive approach to
// finding map keys by suffix, for comparison.
func BenchmarkMap_HasSuffix_NaiveImpl(b *testing.B) {
	if _, ok := os.LookupEnv("BENCH_NAIVE"); !ok {
		b.Skipf("skipping naive implementation benchmark; set env BENCH_NAIVE=1 to run")
	}

	data, size := benchmarkMap(b)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.HasSuffix(k, "Needle") {
					res[k] = v
				}
			}
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if strings.HasSuffix(k, "Benchmark") {
					res[k] = v
				}
			}
		}
	})
}

// BenchmarkMap_Match_NaiveImpl implements a basic, naive approach to
// finding map keys by regex matching, for comparison.
func BenchmarkMap_Match_NaiveImpl(b *testing.B) {
	if _, ok := os.LookupEnv("BENCH_NAIVE"); !ok {
		b.Skipf("skipping naive implementation benchmark; set env BENCH_NAIVE=1 to run")
	}

	data, size := benchmarkMap(b)
	p := regexp.MustCompile(`(n|N)eedle.+`)
	np := regexp.MustCompile(`(b|B)enchmark.+`)

	b.ResetTimer()

	b.Run("match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if p.MatchString(k) {
					res[k] = v
				}
			}
		}
	})

	b.Run("no match "+size, func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			res := make(map[string]any)

			for k, v := range data {
				if np.MatchString(k) {
					res[k] = v
				}
			}
		}
	})
}

func benchmarkSMap(tb testing.TB) (sMap *searchablemap.Map, size string) {
	data, size := benchmarkMap(tb)
	return searchablemap.New(data), size
}

func benchmarkMap(tb testing.TB) (data map[string]any, size string) {
	size = "50k"
	sizes := map[string]int{
		"50k":  50_000,
		"100k": 100_000,
		"250k": 250_000,
		"500k": 500_000,
	}

	if s, ok := os.LookupEnv("BENCH_SIZE"); ok {
		if _, ok := sizes[s]; !ok {
			tb.Fatalf("unknown size %q specified in BENCH_SIZE env variable", s)
		}

		size = s
	}

	name := filepath.Join("testdata", fmt.Sprintf("rand-idents-%s.txt", size))

	f, err := os.Open(name)
	if err != nil {
		tb.Fatalf("error opening testdata file: %v", err)
	}
	defer f.Close()

	data = make(map[string]any, sizes[size])
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		line := string(scanner.Bytes())

		// Make a slice of 128 random bytes to use as value for key.
		randBytes := make([]byte, 128)
		if _, err := rand.Read(randBytes); err != nil {
			tb.Fatalf("error generating random bytes: %v", err)
		}

		data[line] = randBytes
	}

	return data, size
}
